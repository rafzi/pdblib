#include "PDB.h"


int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        printf("invalid arguments\nusage: %s <pdb-file> <type-name>", argv[0]);
        return 1;
    }

    try
    {
        pdb::PDB pdb;

        pdb.loadFromFile(argv[1]);
        auto udt = pdb.getUDT(argv[2]);

        printf("udt name: %s\n", udt->getName().c_str());

        for (const auto& c : *udt)
        {
            auto member = std::dynamic_pointer_cast<pdb::DataMember>(c);
            if (member)
            {
                printf("%s %i\n", c->getName().c_str(), member->getOffset());
            }

            auto memberfunc = std::dynamic_pointer_cast<pdb::MemberFunction>(c);
            if (memberfunc)
            {
                printf("%s %i\n", c->getName().c_str(), memberfunc->getVtOffset());
            }
        }

        return 0;
    }
    catch (std::runtime_error& e)
    {
        printf("runtime_error: %s\n", e.what());
    }
    catch (...)
    {
        printf("unhandled exception\n");
    }

    return 1;
}