#ifndef PDBLIB_SYMBOL_H
#define PDBLIB_SYMBOL_H

#include "dia2.h"
#include <string>
#include <memory>
#include <vector>


namespace pdb {


class Type;


class Symbol
{
public:
    Symbol(IDiaSymbol *symbol);
    virtual ~Symbol();

    void loadChildren();

    std::string getName() const { return m_name; }
    std::shared_ptr<Type> getType() const { return m_type; }

    std::vector<std::shared_ptr<Symbol>>::const_iterator begin() const
    {
        return m_children.begin();
    }
    std::vector<std::shared_ptr<Symbol>>::const_iterator end() const
    {
        return m_children.end();
    }

protected:
    IDiaSymbol *m_symbol;
    DWORD m_symTag;
    uint64_t m_size = 0;
    std::string m_name;
    std::shared_ptr<Type> m_type;
    std::vector<std::shared_ptr<Symbol>> m_children;

};


}

#endif