#include "PDB.h"


using namespace pdb;


std::shared_ptr<Symbol> pdb::SymbolFromDIA(IDiaSymbol *symbol)
{
    std::shared_ptr<Symbol> result = nullptr;

    DWORD symTag;
    auto hr = symbol->get_symTag(&symTag);
    if (FAILED(hr))
    {
        throw std::runtime_error("get_symTag failed");
    }

    switch (symTag)
    {
    case SymTagData:
    {
        // data
        auto dataMember = std::make_shared<DataMember>(symbol);
        result = dataMember;
        break;
    }

    case SymTagTypedef:
    case SymTagVTable:
        // symbol type
        break;

    case SymTagEnum:
    case SymTagUDT:
        // udt
        result = std::make_shared<UserDefinedType>(symbol);
        result->loadChildren();

        break;

    case SymTagFunction:
        // func
        result = std::make_shared<MemberFunction>(symbol);
        break;

    case SymTagPointerType:
        // name & type
        break;

    case SymTagArrayType:
    case SymTagBaseType:
    case SymTagFunctionArgType:
    case SymTagUsingNamespace:
    case SymTagCustom:
    case SymTagFriend:
        // name & symbol type
        break;

    case SymTagVTableShape:
    case SymTagBaseClass:
        // bases
        break;

    case SymTagFunctionType:
        // type
        break;

    case SymTagThunk:
        // thunk
        break;

    default:
        //throw std::runtime_error("invalid symTag");
        break;
    }

    return result;
}


PDB::~PDB()
{
    if (m_global)
    {
        m_global->Release();
    }
    if (m_session)
    {
        m_session->Release();
    }
    if (m_source)
    {
        m_source->Release();
    }

    CoUninitialize();
}

void PDB::loadFromFile(const std::string& fileName)
{
    auto hr = CoInitialize(NULL);
    hr = CoCreateInstance(__uuidof(DiaSource), NULL, CLSCTX_INPROC_SERVER, __uuidof(IDiaDataSource), (void**)&m_source);
    if (FAILED(hr))
    {
        throw std::runtime_error("CoCreateInstance failed");
    }

    std::wstring fileNameW(fileName.begin(), fileName.end());
    m_source->loadDataFromPdb(fileNameW.c_str());
    if (FAILED(hr))
    {
        throw std::runtime_error("loadDataFromPdb failed");
    }

    hr = m_source->openSession(&m_session);
    if (FAILED(hr))
    {
        throw std::runtime_error("openSession failed");
    }

    hr = m_session->get_globalScope(&m_global);
    if (FAILED(hr))
    {
        throw std::runtime_error("get_globalScope failed");
    }
}

std::shared_ptr<UserDefinedType> PDB::getUDT(const std::string& typeName)
{
    std::shared_ptr<UserDefinedType> result = nullptr;

    IDiaEnumSymbols *enumSymbols;
    std::wstring typeNameW(typeName.begin(), typeName.end());
    auto hr = m_global->findChildren(SymTagUDT, typeNameW.c_str(), nsfCaseSensitive, &enumSymbols);
    if (FAILED(hr))
    {
        throw std::runtime_error("findChildren failed");
    }

    IDiaSymbol *symbol;
    ULONG celt = 0;
    if (SUCCEEDED(enumSymbols->Next(1, &symbol, &celt)) && celt == 1)
    {
        result = std::dynamic_pointer_cast<UserDefinedType>(SymbolFromDIA(symbol));

        symbol->Release();
    }
    else
    {
        throw std::runtime_error("typeName not found");
    }

    enumSymbols->Release();

    return result;
}