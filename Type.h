#ifndef PDBLIB_TYPE_H
#define PDBLIB_TYPE_H

#include "Symbol.h"


namespace pdb {


class Type : public Symbol
{
public:
    Type(IDiaSymbol *symbol);

    std::string getTypeName() const { return m_typeName; }

protected:
    std::string m_typeName;
    bool m_const = false;
    bool m_volatile = false;
    bool m_unaligned = false;

};


}

#endif