#include "Symbol.h"
#include "PDB.h"


using namespace pdb;


static std::string BstrToString(BSTR bstr)
{
    std::string result;

    int len = SysStringLen(bstr);
    int newLen = WideCharToMultiByte(CP_ACP, 0, bstr, len, 0, 0, NULL, NULL);
    if (newLen > 0)
    {
        result.resize(newLen);
        WideCharToMultiByte(CP_ACP, 0, bstr, len, &result[0], newLen, NULL, NULL);
    }
    else
    {
        result += "<string conversion failed>";
    }

    return result;
}


Symbol::Symbol(IDiaSymbol *symbol)
    : m_symbol(symbol)
{
    m_symbol->AddRef();

    auto hr = m_symbol->get_symTag(&m_symTag);
    if (FAILED(hr))
    {
        throw std::runtime_error("get_symTag failed");
    }


    ULONGLONG length;
    hr = m_symbol->get_length(&length);
    if (hr == S_OK)
    {
        m_size = length;
    }


    BSTR bstrUndName;
    hr = m_symbol->get_undecoratedName(&bstrUndName);
    if (hr != S_OK)
    {
        BSTR bstrName;
        hr = m_symbol->get_name(&bstrName);
        if (hr != S_OK)
        {
            m_name = "(none)";
        }
        else
        {
            m_name = BstrToString(bstrName);
            SysFreeString(bstrName);
        }
    }
    else
    {
        m_name = BstrToString(bstrUndName);
        SysFreeString(bstrUndName);
    }


    /*IDiaSymbol *type;
    hr = m_symbol->get_type(&type);
    if (hr == S_OK)
    {
    m_type = std::make_shared<Type>(type);

    type->Release();
    }*/
}

Symbol::~Symbol()
{
    m_symbol->Release();
}

void Symbol::loadChildren()
{
    IDiaEnumSymbols *enumChildren;
    auto hr = m_symbol->findChildren(SymTagNull, NULL, nsNone, &enumChildren);
    if (FAILED(hr))
    {
        throw std::runtime_error("findChildren failed");
    }

    IDiaSymbol *child;
    ULONG celt = 0;
    while (SUCCEEDED(enumChildren->Next(1, &child, &celt)) && celt == 1)
    {
        m_children.push_back(SymbolFromDIA(child));

        child->Release();
    }

    enumChildren->Release();
}