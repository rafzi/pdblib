#ifndef PDBLIB_PDB_H
#define PDBLIB_PDB_H

#include "UserDefinedType.h"
#include "DataMember.h"
#include "MemberFunction.h"
#include "Type.h"


namespace pdb {


std::shared_ptr<Symbol> SymbolFromDIA(IDiaSymbol *symbol);


class PDB
{
public:
    ~PDB();

    void loadFromFile(const std::string& fileName);

    std::shared_ptr<UserDefinedType> getUDT(const std::string& typeName);

private:
    IDiaDataSource *m_source = nullptr;
    IDiaSession *m_session = nullptr;
    IDiaSymbol *m_global = nullptr;

};

}

#endif