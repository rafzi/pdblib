#include "MemberFunction.h"


using namespace pdb;


MemberFunction::MemberFunction(IDiaSymbol *symbol)
    : Symbol(symbol)
{
    DWORD vtOffset;
    auto hr = m_symbol->get_virtualBaseOffset(&vtOffset);
    if (hr == S_OK)
    {
        m_vtOffset = vtOffset;
    }
}