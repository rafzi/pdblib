#ifndef PDBLIB_MEMBERFUNCTION_H
#define PDBLIB_MEMBERFUNCTION_H

#include "Symbol.h"


namespace pdb {


class MemberFunction : public Symbol
{
public:
    MemberFunction(IDiaSymbol *symbol);

    uintptr_t getVtOffset() const { return m_vtOffset; }

protected:
    uintptr_t m_vtOffset = 0;

};


}

#endif