#include "DataMember.h"


using namespace pdb;


DataMember::DataMember(IDiaSymbol *symbol)
    : Symbol(symbol)
{
    LONG offset;
    auto hr = m_symbol->get_offset(&offset);
    if (SUCCEEDED(hr))
    {
        m_offset = offset;
    }

    DWORD bitPosition;
    hr = m_symbol->get_bitPosition(&bitPosition);
    if (SUCCEEDED(hr))
    {
        m_bitPosition = bitPosition;
    }
}