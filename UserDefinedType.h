#ifndef PDBLIB_USERDEFINEDTYPE_H
#define PDBLIB_USERDEFINEDTYPE_H

#include "Symbol.h"


namespace pdb {


class UserDefinedType : public Symbol
{
public:
    UserDefinedType(IDiaSymbol *symbol) : Symbol(symbol) { }

protected:

};


}

#endif