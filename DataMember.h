#ifndef PDBLIB_DATAMEMBER_H
#define PDBLIB_DATAMEMBER_H

#include "Symbol.h"


namespace pdb {


class DataMember : public Symbol
{
public:
    DataMember(IDiaSymbol *symbol);

    uintptr_t getOffset() const { return m_offset; }

protected:
    uintptr_t m_offset = 0;
    int m_bitPosition = 0;

};


}

#endif