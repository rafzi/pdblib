#include "Type.h"


using namespace pdb;


static const char *g_baseTypeNames[] =
{
    "<NoType>",                         // btNoType = 0,
    "void",                             // btVoid = 1,
    "char",                             // btChar = 2,
    "wchar_t",                          // btWChar = 3,
    "signed char",
    "unsigned char",
    "int",                              // btInt = 6,
    "unsigned int",                     // btUInt = 7,
    "float",                            // btFloat = 8,
    "<BCD>",                            // btBCD = 9,
    "bool",                             // btBool = 10,
    "short",
    "unsigned short",
    "long",                             // btLong = 13,
    "unsigned long",                    // btULong = 14,
    "__int8",
    "__int16",
    "__int32",
    "__int64",
    "__int128",
    "unsigned __int8",
    "unsigned __int16",
    "unsigned __int32",
    "unsigned __int64",
    "unsigned __int128",
    "<currency>",                       // btCurrency = 25,
    "<date>",                           // btDate = 26,
    "VARIANT",                          // btVariant = 27,
    "<complex>",                        // btComplex = 28,
    "<bit>",                            // btBit = 29,
    "BSTR",                             // btBSTR = 30,
    "HRESULT"                           // btHresult = 31
};


Type::Type(IDiaSymbol *symbol)
    : Symbol(symbol)
{
    BOOL isSet;
    auto hr = m_symbol->get_constType(&isSet);
    if (SUCCEEDED(hr))
    {
        m_const = isSet ? true : false;
        if (m_const)
        {
            m_typeName += "const ";
        }
    }

    hr = m_symbol->get_volatileType(&isSet);
    if (SUCCEEDED(hr))
    {
        m_volatile = isSet ? true : false;
        if (m_volatile)
        {
            m_typeName += "volatile ";
        }
    }

    hr = m_symbol->get_unalignedType(&isSet);
    if (SUCCEEDED(hr))
    {
        m_unaligned = isSet ? true : false;
        if (m_unaligned)
        {
            m_typeName += "__unaligned ";
        }
    }

    switch (m_symTag)
    {
    case SymTagBaseType:
    {
        DWORD baseType;
        hr = m_symbol->get_baseType(&baseType);
        if (FAILED(hr))
        {
            throw std::runtime_error("get_baseType failed");
        }

        switch (baseType)
        {
        case btUInt:
            m_typeName += "unsigned ";
            // Fall through.

        case btInt:
            switch (m_size)
            {
            case 1:
                if (baseType == btInt)
                {
                    m_typeName += "signed ";
                }
                m_typeName += "char";
                break;

            case 2:
                m_typeName += "short";
                break;

            case 4:
                m_typeName += "int";
                break;

            case 8:
                m_typeName += "__int64";
                break;

            default:
                m_typeName += "unklen_int";
            }
            break;

        case btFloat:
            if (m_size == 4)
                m_typeName += "float";
            else if (m_size == 8)
                m_typeName += "double";
            else
                m_typeName += "unklen_float";

        default:
            m_typeName += g_baseTypeNames[baseType];
        }
    }
    }
}